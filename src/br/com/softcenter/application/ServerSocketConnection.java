package br.com.softcenter.application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ServerSocketConnection extends Thread {
	private static List<ThreadCliente> CLIENTES;
	private static ServerSocket serverSocket;
	private static ServerSocketConnection serverSocketConn;
	
	public static ServerSocketConnection getSocketConn(){
		if(serverSocketConn == null){
			serverSocketConn = new ServerSocketConnection();
		}
		return serverSocketConn;
	}
	
	@Override
	public void run() {
		super.run();

		while(true){
			Socket cliente;
			try {
				cliente = serverSocket.accept();
				ThreadCliente threadCliente = new ThreadCliente(cliente);
				threadCliente.start();
				CLIENTES.add(threadCliente);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
	}
	
	public ServerSocket getServerSocket(){
		return serverSocket;
	}

	private ServerSocketConnection(){
		CLIENTES = new ArrayList<ThreadCliente>();
		
		try {
			System.out.println("Servidor iniciado.");
			serverSocket = new ServerSocket(2015);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendToAll(String mensagem){
		for(ThreadCliente threadClient : CLIENTES){
			if(threadClient.getSocket().isConnected()){
				threadClient.sendMenssage(mensagem);
			}else{
				try {
					threadClient.getSocket().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				CLIENTES.remove(threadClient);
			}
			
			System.out.println("CLIENTES: " + CLIENTES.size());
		}
	}
}
