package br.com.softcenter.application;

import java.io.PrintWriter;
import java.net.Socket;

public class ThreadCliente extends Thread {

	private Socket clientSocket;
	private PrintWriter out;
	
	public ThreadCliente(Socket clientSocket) throws Exception{
		this.clientSocket = clientSocket;
		//Pega o stream de saída para o cliente:
		this.out = new PrintWriter(clientSocket.getOutputStream(), true);
		System.out.println("Cliente conectado");
	}
	
	public void sendMenssage(String mensagem){
		out.println(mensagem);
	}
	
	public Socket getSocket(){
		return clientSocket;
	}
	
	@Override
	public void run() {
		super.run();
	}
}
